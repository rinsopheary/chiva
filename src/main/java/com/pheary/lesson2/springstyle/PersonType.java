package com.pheary.lesson2.springstyle;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class PersonType {

    /*
    There are three type of injection
    1. data field
    2. constructor
    3. setter getter
     */

    @Autowired
    @Qualifier("student")
    private Person person;


    public PersonType(){}

//    @Autowired
//    public PersonType(@Qualifier("student") Person person) {
//        this.person = person;
//    }
    public PersonType(Person person) {
        this.person = person;
    }

    public Person getPerson() {
        return person;
    }


    public void setPerson(@Qualifier("student") Person person) {
        this.person = person;
    }

    public void chooseType(){
        this.person.type();
    }

    @PostConstruct
    public void init(){
        System.out.println("Init app");
    }
    @PreDestroy
    public void dest(){
        System.out.println("Destroy");
    }
}
