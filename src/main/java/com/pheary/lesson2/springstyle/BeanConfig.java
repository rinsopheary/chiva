package com.pheary.lesson2.springstyle;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig {
    @Bean
    public Student student(){
        return new Student();
    }
    @Bean
    public Employee employee(){
        return new Employee();
    }

    @Bean
    public Test test(){
        return new Test();
    }
}
