package com.pheary.lesson2;


import com.pheary.lesson2.springstyle.PersonType;
import com.pheary.lesson2.springstyle.Student;
import com.pheary.lesson2.springstyle.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Lesson2Application {

    public static void main(String[] args) {


        //        ApplicationContext context = SpringApplication.run(Lesson2Application.class, args);

        BeanFactory context = SpringApplication.run(Lesson2Application.class, args);

        PersonType personType = context.getBean("personType", PersonType.class);

//        Student student = context.getBean("student", Student.class);

//        personType.setPerson(student);
        personType.chooseType();
//        ((ConfigurableApplicationContext) context).close();

    }

}
