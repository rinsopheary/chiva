package com.pheary.lesson2.javastyle;

public class PersonType {
    private Person person;

    public PersonType(){}

    public PersonType(Person person) {
        this.person = person;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public void chooseType(){
        this.person.type();
    }
}
