package com.pheary.lesson2.javastyle;

public class MainPerson {
    public static void main(String[] args) {
        PersonType personType = new PersonType();
        personType.setPerson(new Student());
        personType.chooseType();
    }
}
